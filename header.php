<?php

	$text_color = get_theme_mod( 'cc_text_color' );
	$header_toggle = get_theme_mod( 'cc_header_toggle' );

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php bloginfo('name'); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class('text-center'); ?>>
	<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column <?php echo esc_attr( $text_color ); ?>">
		<header class="masthead mb-auto">
			<div class="inner<?php if ( $header_toggle == 0 ) : ?> hide<?php endif; ?>">
				<h3 class="<?php if ( has_nav_menu( 'header-menu' ) ) : ?>masthead-brand <?php endif; ?>text-center"><?php bloginfo('name'); ?></h3>
				<?php if ( has_nav_menu( 'header-menu' ) ) : ?>
				<nav class="nav nav-masthead justify-content-center">
					<?php 
						wp_nav_menu( array( 
							'theme_location' 	=> 'header-menu',
							'container'			=> '',
							'menu_class'		=> 'navbar-nav flex-row',
						) ); 
					?>
				</nav>
			<?php endif; ?>
			</div>
		</header>
		<main role="main" class="inner cover">