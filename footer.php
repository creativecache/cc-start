		</main>

		<footer class="mastfoot mt-auto">
			<div class="inner">
				<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved. | <a href="https://themes.creativecache.co/start" target="_blank">CC Start Theme</a> by <a href="https://creativecache.co/" target="_blank">Creative Cache</a></p>
			</div>
		</footer>

	</div>

	<?php wp_footer(); ?>

</body>
</html>