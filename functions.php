<?php

//Enqueue scripts and styles
function citadel_adding_scripts() {
	wp_enqueue_style( 'bootstrap-styles', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' );
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');

	wp_enqueue_script( 'bootstrap-jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'bootstrap-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'bootstrap-scripts', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );

	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'citadel_adding_scripts' );

//Register navigation menu
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );


function cc_customize_register( $wp_customize ) {
    $wp_customize->add_section( 'cc_section' , array(
            'title'      => __( 'CC Start Settings', 'cc' ),
            'description' => 'Modify the settings for the CC Start theme',
            'priority'   => 40,
    ));

    $wp_customize->add_setting( 'cc_background_color', array(
    	'default' => '#333333'
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'cc_background_color', array(
            'label'      => __( 'Background Color', 'cc' ),
            'section'    => 'cc_section',
            'settings'   => 'cc_background_color',
    )));

    $wp_customize->add_setting( 'cc_text_color', array(
        'default' => 'light-text'
    ));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cc_text_color', array(
            'label'     => __( 'Text Color', 'cc' ),
            'section'   => 'cc_section',
            'settings'  => 'cc_text_color',
            'type'      => 'radio',
            'choices'   => array(
                    'light-text'    => 'Light',
                    'dark-text'     => 'Dark',
            ),
    )));

    $wp_customize->add_setting( 'cc_header_toggle', array(
        'default' => 1
    ));

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cc_header_toggle', array(
            'label'     => __( 'Show Header', 'cc' ),
            'section'   => 'cc_section',
            'settings'  => 'cc_header_toggle',
            'type'      => 'checkbox',
            'choices'   => array(
                    'show'    => 'Show',
            ),
    )));
}
add_action( 'customize_register', 'cc_customize_register' );

function cc_customizer_head_styles() {
	$link_color = get_theme_mod( 'cc_background_color' ); 
	
	if ( $link_color != '#333333' ) :
	?>
		<style type="text/css">
			body {
				background-color: <?php echo $link_color; ?>;
			}
		</style>
	<?php
	endif;
}
add_action( 'wp_head', 'cc_customizer_head_styles' );